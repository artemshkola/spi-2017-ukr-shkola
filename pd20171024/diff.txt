NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda      8:0    0 931.5G  0 disk 
├─sda4   8:4    0 422.3G  0 part 
├─sda2   8:2    0    16M  0 part 
├─sda9   8:9    0   7.6G  0 part [SWAP]
├─sda7   8:7    0   4.7G  0 part /home
├─sda5   8:5    0    25G  0 part 
├─sda3   8:3    0 453.7G  0 part 
├─sda1   8:1    0   260M  0 part /boot/efi
├─sda8   8:8    0  14.4G  0 part /
└─sda6   8:6    0  1000M  0 part 
