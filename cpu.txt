Architektura:          x86_64
Tryb(y) pracy CPU:     32-bit, 64-bit
Kolejność bajtów:   Little Endian
CPU:                   4
Lista aktywnych CPU:   0-3
Wątków na rdzeń:    1
Rdzeni na gniazdo:     4
Gniazd:                1
Węzłów NUMA:        1
ID producenta:         GenuineIntel
Rodzina CPU:           6
Model:                 58
Nazwa modelu:          Intel(R) Core(TM) i5-3470 CPU @ 3.20GHz
Wersja:                9
CPU MHz:               1600.000
CPU max MHz:           3600,0000
CPU min MHz:           1600,0000
BogoMIPS:              6388.69
Wirtualizacja:         VT-x
Cache L1d:             32K
Cache L1i:             32K
Cache L2:              256K
Cache L3:              6144K
Procesory węzła NUMA 0:0-3
Flagi:                 fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx rdtscp lm constant_tsc arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc aperfmperf eagerfpu pni pclmulqdq dtes64 monitor ds_cpl vmx smx est tm2 ssse3 cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm epb tpr_shadow vnmi flexpriority ept vpid fsgsbase smep erms xsaveopt dtherm ida arat pln pts
