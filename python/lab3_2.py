import turtle
def draw_square(t,length):
    for j in range(5):
        if j!=0:
            t.left(145)
            t.forward(length)
        else:
            t.left(105)
            t.forward(length)

def main():
    win=turtle.Screen()
    win.bgcolor("lightgreen")
    bob=turtle.Turtle()
    bob.color("blue")
    bob.pensize(3)
    draw_square(bob,100)
    win.mainloop()
main()
