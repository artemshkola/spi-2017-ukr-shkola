import turtle

def draw_square(t, length):
    for j in range(12):
        t.penup()
        t.forward(length)
        t.backward(40)
        t.pendown()
        t.forward(10)
        t.penup()
        t.forward(30)
        t.pendown()
        t.stamp()
        t.heading()
        t.penup()
        t.backward(length)
        t.left(30)
        t.pendown()
def main():
    win=turtle.Screen()
    win.bgcolor("lightgreen")
    bob=turtle.Turtle()
    bob.color("blue")
    bob.shape("turtle")
    bob.pensize(3)
    draw_square(bob,150)
    win.mainloop()
main()
